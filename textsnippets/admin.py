from django.contrib import admin

# Register your models here.

from authentication.models import User
from .models import Tags, Textsnippet

admin.site.register(User)
admin.site.register(Tags)
admin.site.register(Textsnippet)